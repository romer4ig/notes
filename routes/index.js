var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var router = express.Router();


router.get('/', function (req, res) {
    res.redirect('/notes');
});

router.get('/register', function(req, res) {
    res.render('account/register', { });
});

router.post('/register', function(req, res) {
    Account.register(new Account({ username : req.body.username }), req.body.password, function(err, account) {
        if (err) {
            return res.render('account/register', { account : account });
        }

        passport.authenticate('local')(req, res, function () {
            res.redirect('/notes');
        });
    });
});

router.get('/login', function(req, res) {
    res.render('account/login', { user : req.user });
});

router.post('/login', passport.authenticate('local'), function(req, res) {
    res.redirect('/notes');
});

router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});


module.exports = router;
