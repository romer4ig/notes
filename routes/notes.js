var express = require('express');
var Note = require('../models/note');
var router = express.Router();


router.get('/', function (req, res) {
    Note.find({user_id: req.user._id}, function (err, notes) {
        res.render('notes/index',{notes:notes});
    });

});

router.post('/add', function (req, res) {
    var _note = new Note({
        note: req.body.note,
        user_id: req.user._id
    });
    _note.save(function (err) {
        if (err) return console.log(err);
        res.redirect('/notes')
    })
});
router.get('/delete', function (req, res) {
    console.log(req.query.id)
    Note.remove({_id:req.query.id},function (err,removed) {
        if (err) return console.log(err);
        console.log('removed',removed)
        res.redirect('/notes')
    })
});

module.exports = router;
