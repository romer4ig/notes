var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Note = new Schema({
    note: String,
    user_id: String
});


module.exports = mongoose.model('Note', Note);
